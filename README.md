# Selenium test automation challenge

## Tools for testing

- Java language, jdk1.8.0_231
- IDE: IntelliJ.
- Project management and construction: Maven
- BDD Framework: cucumber.
- TDD Framework: jUnit.
- Design pattern: Page Object Model
- Web application testing environment: Selenium

## Steps to Run Project

-   Project developed in Windows as ios
-	Have java 8 or higher installed.
-	Use or download Intellij Community Edition.
-	Open project in IDE.


## Identify Type of Project

-	At the root of the Project we right click Add Framework support and select Maven
-	Run TestRunner class.

# Project Structure

-	In the **register.feature** we have two test case scenarios, one to verify the registration attempt with weak password and another to verify the correct registration when using a strong password.
-	In the **stepsDefinitions package** you will find the steps class with the description of each of the steps, this class inherits its properties of the **stepDefBase class** of the commons package, here the different configurations are included at the beginning and end of each test **(including the initiation and closing of the Browser)**.
-	In the **Utils package**, there are some common functions for reusing selenium and also for handling exceptions.
-	 In the **runner package** is the **TestRunner class**, where we have the connection between the features created and the steps.
-	In the **pageObject package**, we define the classes assigned to the different views treated during the tests **(HomePage, LoginPage, MyAccountPage)**.
-	The **managers package** is used for the **PageObjectManager class**, where all views used in the model are initialized.
