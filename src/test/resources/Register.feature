Feature: Automated Tests
  Description: The purpose of this feature is to test the page registration.

  Background:
    Given user is on Home Page

    @Run
  Scenario Outline: User tries to register with a weak password
    When he goes to My account option
    And verify my account page
    And fill the registration form with <username> <email address> and <password>
    And verify weak password message
    And verify Register button is disabled

    Examples:

      | username    | email address       | password  |
      | testing25   | testing@gmail.com   | 627458765 |

  Scenario Outline: User tries to register with a strong password
    When he goes to My account option
    And fill the registration form with <username> <email address> and <password>
    And press Register button
    And verify login page

  Examples:

    | username            | email address                 | password             |
    | testingselenium03    | testingselenium03@gmail.com  | TestingSelenium2510* |

