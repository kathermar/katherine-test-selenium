package utils;

import cucumber.api.PendingException;
import cucumber.api.Scenario;
import utils.exceptions.ExceptionSelenium;
import org.openqa.selenium.*;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class TestUtils {

    public static Map<String, byte[]> listScreenShot = new HashMap<>();
    private static TestUtils instance = null;

    public static TestUtils getInstance() {
        return instance = (instance == null) ? instance = new TestUtils() : instance;
    }

    public void controllingExceptions(final Throwable e) throws Throwable {
        String linea = "";
        if (e instanceof AssertionError) {
            StackTraceElement[] traza = e.getStackTrace();
            for (StackTraceElement trazaActual : traza) {
                if (trazaActual.toString().contains("pageObject")) {
                    linea = trazaActual.toString();
                }
            }
            new ExceptionSelenium(new AssertionError(e.getMessage() + "\n\ten " + linea));
            throw new AssertionError(e.getMessage() + "\n\ten " + linea);
            //}
        } else if (e instanceof InvalidSelectorException) {
            StackTraceElement[] traza = e.getStackTrace();
            for (StackTraceElement trazaActual : traza) {
                if (trazaActual.toString().contains("pageObject")) {
                    linea = trazaActual.toString();
                }
            }
            new ExceptionSelenium(new InvalidSelectorException(e.getMessage() + "\n\ten " + linea));
            throw new InvalidSelectorException(e.getMessage() + "\n\ten " + linea);
        } else if (e instanceof NoSuchElementException) {
            StackTraceElement[] traza = e.getStackTrace();
            for (StackTraceElement trazaActual : traza) {
                if (trazaActual.toString().contains("pageObject")) {
                    linea = trazaActual.toString();
                }
            }
            new ExceptionSelenium(new NoSuchElementException(e.getMessage() + "\n\ten " + linea));
            throw new NoSuchElementException(e.getMessage() + "\n\ten " + linea);
        } else if (e instanceof TimeoutException) {
            StackTraceElement[] traza = e.getStackTrace();
            for (StackTraceElement trazaActual : traza) {
                if (trazaActual.toString().contains("pageObject")) {
                    linea = trazaActual.toString();
                }
            }
            new ExceptionSelenium(new TimeoutException(e.getMessage() + "\n\ten " + linea));
            throw new TimeoutException(e.getMessage() + "\n\ten " + linea);
        } else if (e instanceof PendingException) {
            StackTraceElement[] traza = e.getStackTrace();
            for (StackTraceElement trazaActual : traza) {
                if (trazaActual.toString().contains("pageObject")) {
                    linea = trazaActual.toString();
                }
            }

            new ExceptionSelenium(new PendingException(e.getMessage() + "\n\ten " + linea));
            throw new PendingException(e.getMessage() + "\n\ten " + linea);
        } else if (e instanceof NullPointerException) {
            StackTraceElement[] traza = e.getStackTrace();
            for (StackTraceElement trazaActual : traza) {
                if (trazaActual.toString().contains("pageObject")) {
                    linea = trazaActual.toString();
                }
            }

            new ExceptionSelenium(new NullPointerException(e.getMessage() + "\n\ten " + linea));
             throw new NullPointerException(e.getMessage() + "\n\ten " + linea);
        } else if (e instanceof Exception) {
            StackTraceElement[] traza = e.getStackTrace();
            for (StackTraceElement trazaActual : traza) {
                if (trazaActual.toString().contains("pageObject")) {
                    linea = trazaActual.toString();
                }
            }
            new ExceptionSelenium(new Exception(e.getMessage() + "\n\ten " + linea));
            throw new Exception(e.getMessage() + "\n\ten " + linea);
        }
    }

    public void doScreenShot(Scenario scenary, WebDriver driver, String reason) {
        try {
            byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            if (screenshot.length > 500000) {
                screenshot = scale(screenshot, 1796, 940);
            }
            scenary.embed(screenshot, "image/png");
            String path = "screenshot_" + reason;
            listScreenShot.put(path, screenshot);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private byte[] scale(byte[] fileData, int width, int height) {
        ByteArrayInputStream in = new ByteArrayInputStream(fileData);
        try {
            BufferedImage img = ImageIO.read(in);
            if (height == 0) {
                height = (width * img.getHeight()) / img.getWidth();
            }
            if (width == 0) {
                width = (height * img.getWidth()) / img.getHeight();
            }
            Image scaledImage = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
            BufferedImage imageBuff = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            imageBuff.getGraphics().drawImage(scaledImage, 0, 0, new Color(0, 0, 0), null);
            java.io.ByteArrayOutputStream buffer = new java.io.ByteArrayOutputStream();
            ImageIO.write(imageBuff, "jpg", buffer);
            return buffer.toByteArray();
        } catch (IOException e) {
            throw new AssertionError(e.getMessage());
        }
    }

    public BigDecimal parse(final String amount) {
        final NumberFormat format = NumberFormat.getNumberInstance(Locale.getDefault());

        BigDecimal parse = null;
        if (format instanceof DecimalFormat) {
            ((DecimalFormat) format).setParseBigDecimal(true);
        }
        try {
            parse = (BigDecimal) format.parse(amount.replaceAll("[^\\d.,]", ""));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return parse;
    }
}
