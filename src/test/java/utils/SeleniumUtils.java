package utils;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static java.util.concurrent.TimeUnit.SECONDS;

public class SeleniumUtils {

    private static SeleniumUtils instance = null;

    public static SeleniumUtils getInstance() {
        return instance = (instance == null) ? instance = new SeleniumUtils() : instance;
    }

    public final void espera(long millis) throws Exception {
        Thread.sleep(millis);
    }

    public final boolean webElementIsDisplayed(WebElement element) {
        boolean b = false;

        try {
            b = element.isDisplayed();
        } catch (Exception e) {
            // No se quiere mostrar error en el reporte
        }
        return b;
    }

    public boolean existsElement(final WebElement elemento) {
        try {
            elemento.getTagName();
        } catch (NoSuchElementException e) {
            return false;
        }
        return true;
    }

    public boolean existsElementXpath(final WebElement elemento, final String xpath) {
        try {
            elemento.findElement(By.xpath(xpath)).getTagName();
        } catch (NoSuchElementException e) {
            return false;
        }
        return true;
    }

    public void clickWebElement(WebDriver driver, final WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(element));
        element.click();
    }

    public void clickWebElementXpath(WebDriver driver, final WebElement element, final String xpath) {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(element.findElement(By.xpath(xpath))));
        element.findElement(By.xpath(xpath)).click();
    }

    public boolean isElementPresent(WebDriver driver, WebElement element, int timeout) {
        boolean result;
        try {
            driver.manage().timeouts().implicitlyWait(timeout, SECONDS);
            result = isElementPresent(element);
            driver.manage().timeouts().implicitlyWait(10, SECONDS);
        } catch (NoSuchElementException e) {
            result = false;
        }
        return result;
    }

    public boolean isElementPresent(WebDriver driver, By by, int timeout) {
        boolean result;
        driver.manage().timeouts().implicitlyWait(timeout, SECONDS);
        result = isElementPresent(driver, by);
        driver.manage().timeouts().implicitlyWait(10, SECONDS);
        return result;
    }

    public boolean isElementPresent(WebDriver driver, By by) {
        try {
            return !driver.findElements(by).isEmpty();
        } catch (StaleElementReferenceException sere) {
            return false;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public boolean isElementPresent(WebDriver driver, WebElement element, String path, int timeout) {
        boolean result;
        try {
            driver.manage().timeouts().implicitlyWait(timeout, SECONDS);
            WebElement webElement = element.findElement(By.xpath(path));
            result = isElementPresent(webElement);
            driver.manage().timeouts().implicitlyWait(20, SECONDS);
        } catch (NoSuchElementException e) {
            result = false;
        }
        return result;
    }

    public void scrollIntoElement(WebDriver driver, WebElement element) throws Throwable{
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        try {
            jse.executeScript("arguments[0].scrollIntoView(true);", element);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void scrollIntoElementXpath(WebDriver driver, WebElement element, String path) {
        WebElement webElement = element.findElement(By.xpath(path));
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        try {
            jse.executeScript("arguments[0].scrollIntoView(true);", element);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isElementPresent(WebElement lWElement) {
        try {
            return lWElement.isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public boolean elementoNoPresente(WebDriver driver, WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        try {
            wait.until(ExpectedConditions.invisibilityOf(element));
        } catch (NoSuchElementException nsee) {
            return true;
        }

        return false;
    }
}
