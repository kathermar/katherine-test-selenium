package utils.exceptions;

import cucumber.api.PendingException;
import org.openqa.selenium.InvalidSelectorException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;

public class ExceptionSelenium {

    private static String trazaError;

    public ExceptionSelenium(final PendingException traza) {
        String trazaCreada = trazaError(traza);
        this.trazaError = trazaCreada;
    }

    public ExceptionSelenium(final Exception traza) {
        String trazaCreada = trazaError(traza);
        this.trazaError = trazaCreada;
    }

    public ExceptionSelenium(final InvalidSelectorException traza) {
        String trazaCreada = trazaError(traza);
        this.trazaError = trazaCreada;
    }

    public ExceptionSelenium(final NoSuchElementException traza) {
        String trazaCreada = trazaError(traza);
        this.trazaError = trazaCreada;
    }

    public ExceptionSelenium(final TimeoutException traza) {
        String trazaCreada = trazaError(traza);
        this.trazaError = trazaCreada;
    }

    public ExceptionSelenium(final NullPointerException traza) {
        String trazaCreada = trazaError(traza);
        this.trazaError = trazaCreada;
    }

    public ExceptionSelenium(final AssertionError traza) {
        String trazaCreada = trazaError(traza);
        this.trazaError = trazaCreada;
    }

    public ExceptionSelenium(final String traza) {
        this.trazaError = traza;
    }

    public String trazaError(PendingException traza) {
        String trazaSend = "PENDING EXCEPTION: " + traza.getMessage() + "\n";
        StackTraceElement[] trazaArray = traza.getStackTrace();
        for (int i = 0; i < trazaArray.length; i++) {
            trazaSend += "Class: " + trazaArray[i].getClassName() + "--> Method: " + trazaArray[i].getMethodName() + "--> line: " + trazaArray[i].getLineNumber() + "\n";
        }
        return trazaSend;
    }


    public String trazaError(Exception traza) {
        String trazaSend = "EXCEPTION: " + traza.getMessage() + "\n";
        StackTraceElement[] trazaArray = traza.getStackTrace();
        for (int i = 0; i < trazaArray.length; i++) {
            trazaSend += "Class: " + trazaArray[i].getClassName() + "--> Method: " + trazaArray[i].getMethodName() + "--> line: " + trazaArray[i].getLineNumber() + "\n";
        }
        return trazaSend;
    }

    public String trazaError(AssertionError traza) {
        String trazaSend = "ASSERTION ERROR: " + traza.getMessage() + "\n";
        StackTraceElement[] trazaArray = traza.getStackTrace();
        for (int i = 0; i < trazaArray.length; i++) {
            trazaSend += "Class: " + trazaArray[i].getClassName() + "--> Method: " + trazaArray[i].getMethodName() + "--> line: " + trazaArray[i].getLineNumber() + "\n";
        }
        return trazaSend;
    }

    public static String getTrazaError() {
        return trazaError;
    }

    public static void setTrazaError(String trazaError) {
        ExceptionSelenium.trazaError = trazaError;
    }
}
