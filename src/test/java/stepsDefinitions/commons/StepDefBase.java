package stepsDefinitions.commons;


import utils.SeleniumUtils;
import utils.TestUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.util.concurrent.TimeUnit;

import static org.openqa.selenium.firefox.FirefoxDriver.MARIONETTE;

public class StepDefBase {

    protected static WebDriver driver;
    protected static TestUtils testUtils;
    protected static SeleniumUtils seleniumUtils;


    protected void initPageFactory() {
        this.testUtils = TestUtils.getInstance();
        this.seleniumUtils = SeleniumUtils.getInstance();

    }

    protected void initDriver() {
        boolean isHeadLess = Boolean.parseBoolean(System.getProperty("headless"));
        String os = System.getProperty("os.name").toLowerCase();
        //String browser = System.getProperty("browser");
        String browser = "Chrome";

        switch (browser) {
            case "Chrome":
                if (os.contains("win")) {
                    System.setProperty("webdriver.chrome.driver", "drivers" + File.separator + "chromedriver.exe");
                    ChromeOptions chromeOptions = new ChromeOptions();
                    if(isHeadLess) {
                        chromeOptions.addArguments("disable-infobars");
                        chromeOptions.addArguments("--window-size=1920,1080");
                        chromeOptions.addArguments("--disable-gpu");
                        chromeOptions.addArguments("--disable-extensions");
                        chromeOptions.setExperimentalOption("useAutomationExtension", false);
                        chromeOptions.addArguments("--start-maximized");
                        //chromeOptions.setHeadless(isHeadLess);
                        chromeOptions.addArguments("--headless");
                    }
                    driver = new ChromeDriver(chromeOptions);
                } else if (os.contains("nux")) {
                    System.setProperty("webdriver.chrome.driver", "drivers" + File.separator + "linux" + File.separator + "chromedriver");
                    ChromeOptions chromeOptions = new ChromeOptions();
                    if(isHeadLess) {
                        chromeOptions.addArguments("disable-infobars");
                        chromeOptions.addArguments("--window-size=1920,1080");
                        chromeOptions.addArguments("--disable-gpu");
                        chromeOptions.addArguments("--disable-extensions");
                        chromeOptions.setExperimentalOption("useAutomationExtension", false);
                        chromeOptions.addArguments("--start-maximized");
                        //chromeOptions.setHeadless(isHeadLess);
                        chromeOptions.addArguments("--headless");
                    }
                    driver = new ChromeDriver(chromeOptions);
                }
                break;
            case "Firefox":
                if (os.contains("win")) {
                    System.setProperty("webdriver.gecko.driver", "drivers" + File.separator + "geckodriver.exe");
                    DesiredCapabilities desiredCapabilities = DesiredCapabilities.firefox();
                    desiredCapabilities.setCapability(MARIONETTE, true);
                    desiredCapabilities.setCapability(MARIONETTE, true);
                    desiredCapabilities.setJavascriptEnabled(true);
                    desiredCapabilities.setPlatform(Platform.WINDOWS);

                    FirefoxOptions firefoxOptions = new FirefoxOptions(desiredCapabilities);
                    firefoxOptions.setHeadless(isHeadLess);
                    firefoxOptions.setPageLoadStrategy(PageLoadStrategy.EAGER);
                    driver = new FirefoxDriver(firefoxOptions);
                } else if (os.contains("nux")) {
                    System.setProperty("webdriver.gecko.driver", "drivers" + File.separator + "linux" + File.separator + "geckodriver");
                    DesiredCapabilities desiredCapabilities = DesiredCapabilities.firefox();
                    desiredCapabilities.setCapability(MARIONETTE, true);
                    desiredCapabilities.setJavascriptEnabled(true);
                    desiredCapabilities.setPlatform(Platform.LINUX);

                    FirefoxOptions firefoxOptions = new FirefoxOptions(desiredCapabilities);
                    firefoxOptions.setHeadless(isHeadLess);
                    firefoxOptions.setPageLoadStrategy(PageLoadStrategy.EAGER);
                    driver = new FirefoxDriver(firefoxOptions);
                }
                break;
        }

        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }
}
