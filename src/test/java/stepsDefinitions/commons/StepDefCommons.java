package stepsDefinitions.commons;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import utils.exceptions.ExceptionSelenium;


public class StepDefCommons extends StepDefBase {

    private static Scenario scenary;
    private static String notes;

    @Before
    public void setUp(final Scenario scenario) throws Exception {
        beforeClass();
        scenary = scenario;
    }

    public void beforeClass() throws Exception {
        initDriver();
        initPageFactory();
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
    }


}