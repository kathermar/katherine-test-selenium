package stepsDefinitions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import managers.PageObjectManager;
import pageObjects.*;
import stepsDefinitions.commons.StepDefBase;


public class Steps extends StepDefBase {

    HomePage homePage;
    LoginPage loginPage;
    MyAccountPage myAccountPage;
    //ConfigFileReader configFileReader;
    PageObjectManager pageObjectManager;

    @Given("^user is on Home Page$")
    public void user_is_on_Home_Page(){
        pageObjectManager = new PageObjectManager(driver);
        homePage = pageObjectManager.getHomePage();
        homePage.navigateTo_HomePage();
        homePage.clickOn_Cookies();
    }

    @When("he goes to My account option")
    public void he_goes_to_my_account_option() throws  Throwable{
        try {
            myAccountPage = pageObjectManager.getMyAccountPage();
            myAccountPage.clickOn_MyAccount();
        } catch (Throwable e){
            testUtils.controllingExceptions(e);
        }
    }

    @And("^verify my account page$")
    public void verifyMyAccountPage() throws  Throwable{
        try {
            myAccountPage.validateMyAccountPage();
        } catch (Throwable e){
            testUtils.controllingExceptions(e);
        }

    }

    @When("^fill the registration form with ([^\"]*) ([^\"]*) and ([^\"]*)$")
    public void fill_the_registration_form_with_asdaa_asdd_and(String username, String email, String password) throws Throwable {
        try {
            myAccountPage.cleanFields();
            myAccountPage.fillInformation(username,email,password);
        } catch (Throwable e){
            testUtils.controllingExceptions(e);
        }
    }


    @When("^verify weak password message$")
    public void verify_weak_password_message() throws Throwable {
        try {
            myAccountPage.validateMessage();
        } catch (Throwable e){
            testUtils.controllingExceptions(e);
        }
    }

    @When("^verify Register button is disabled$")
    public void verify_Register_button_is_disabled() throws Throwable {
        try {
            myAccountPage.checkDisabledRegister();
            driver.quit();
        } catch (Throwable e){
            testUtils.controllingExceptions(e);
        }
    }

    @When("^press Register button$")
    public void press_Register_button() throws Throwable {
        try {
            myAccountPage.clickOn_Register();
        } catch (Throwable e){
            testUtils.controllingExceptions(e);
        }
    }

    @When("^verify login page$")
    public void verify_my_account_page() throws Throwable {
        try {
            loginPage = pageObjectManager.getLoginPage();
            loginPage.validateLoginPage();
            driver.quit();
        } catch (Throwable e){
            testUtils.controllingExceptions(e);
        }
    }

}