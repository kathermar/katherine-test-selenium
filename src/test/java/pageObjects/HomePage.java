package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;


public class HomePage {

    private WebDriver driver;


    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }


    @FindBy(how = How.CSS, using = "a.woocommerce-store-notice__dismiss-link")
    private WebElement btn_Cookies;

    @FindBy(how = How.CSS, using = ".woocommerce-store-notice.demo_store")
    private WebElement cookies_banner;

    public void clickOn_Cookies() {
        if (cookies_banner.isDisplayed()) {
            btn_Cookies.click();
        }
    }


    public void navigateTo_HomePage() {
        driver.get("http://www.shop.demoqa.com");
    }

}