package pageObjects;

import org.junit.Assert;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.SeleniumUtils;


public class LoginPage {

    private static final int TIME_OUT = 3;
    private WebDriver driver;
    private WebDriverWait wait;
    private SeleniumUtils seleniumUtils = SeleniumUtils.getInstance();

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(this.driver, TIME_OUT);
        PageFactory.initElements(driver, this);
    }

    //Login page id for div
    @FindBy(how = How.CSS, using = "div#login")
    private WebElement login;


    public void validateLoginPage() {
        try {
            wait.until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(login)));
        } catch (TimeoutException te) {
            throw new AssertionError("Login page was not accessed");
        }
    }


}
