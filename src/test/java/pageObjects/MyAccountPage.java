package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.TimeoutException;
import org.junit.Assert;
import utils.SeleniumUtils;
import utils.TestUtils;
import java.util.List;


public class MyAccountPage {

    private static final int TIME_OUT = 3;
    private WebDriver driver;
    private WebDriverWait wait;
    private SeleniumUtils seleniumUtils = SeleniumUtils.getInstance();

    public MyAccountPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(this.driver, TIME_OUT);
        PageFactory.initElements(driver, this);
    }


    //List with home menu options
    @FindBy(how = How.CSS, using = "ul.pull-right li a")
    private List<WebElement> btns_Home;

    //My account page title
    @FindBy(how = How.CSS, using = "h1.page-title")
    private WebElement page_title;

    //inputs for registration form
    @FindBy(how = How.CSS, using = "#reg_username")
    private WebElement txtbx_userName;

    @FindBy(how = How.CSS, using = "#reg_email")
    private WebElement txtbx_email;

    @FindBy(how = How.CSS, using = "#reg_password")
    private WebElement txtbx_password;

    @FindBy(how = How.CSS, using = ".woocommerce-Button.button")
    private WebElement btn_Register;

    @FindBy(how = How.CSS, using = "button.woocommerce-Button.button.disabled")
    private WebElement btn_Register_disabled;

    @FindBy(how =  How.CSS, using = ".woocommerce-password-strength.bad")
    private WebElement weakPassword_message;


    //My account option position
    public static final int MY_ACCOUNT = 1;

    public void clickOn_MyAccount() throws InterruptedException {
        Thread.sleep(500);
        try {
            wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(btns_Home.get(MY_ACCOUNT))));
        } catch (TimeoutException te) {
            throw new AssertionError("my Account button is not available");
        }
        btns_Home.get(MY_ACCOUNT).click();
    }


    public void validateMyAccountPage(){
        try {
            wait.until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(page_title)));
        } catch (TimeoutException te) {
            throw new AssertionError("My account page was not accessed");
        }

    }

    public void cleanFields() {
        txtbx_userName.clear();
        txtbx_email.clear();
        txtbx_password.clear();
    }

    public void fillInformation(String username,String email, String password) throws InterruptedException {
        Thread.sleep(2000);

        txtbx_userName.sendKeys(username);
        txtbx_email.sendKeys(email);
        txtbx_password.sendKeys(password);

        Thread.sleep(2000);

    }

    public void validateMessage(){
        Assert.assertTrue("Weak password message not displayed", (seleniumUtils.webElementIsDisplayed(weakPassword_message)));
    }

    public void checkDisabledRegister(){

        Assert.assertTrue("Button Register is not disabled", (seleniumUtils.webElementIsDisplayed(btn_Register_disabled)));

    }

    public void clickOn_Register() throws InterruptedException {
        Thread.sleep(500);
        try {
            wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(btn_Register)));
        } catch (TimeoutException te) {
            throw new AssertionError("Register button is not available");
        }
        btn_Register.click();
    }
}
